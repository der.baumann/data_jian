% This Function made for compare the RMSE bewteen old and new nest.
% Jian Hu
% 04/02/2022
function [nestmat,Value1] = ObjFun(nestmat,new_nestmat,RPS_new,RP_new,newOCV,Value1,n_max)

faktor = 0.5;          % weigt of relaxation point in one bin.(for RPS is relativ 1)
a = new_nestmat(1,1);
b = new_nestmat(1,2);
c = new_nestmat(1,3);
d = new_nestmat(1,4);
fnew = [];                       % as like Value1 in Finess Function.mat 
OCV_x = (newOCV(:,1)+a).*b;      % Use the formula 4.1 and 4.2 in the thesis 
OCV_y = (newOCV(:,2)+c).*d;
OCV_rebuild = [OCV_x,OCV_y];

for i = 1 : n_max
    OCV_V_RP = [];
    OCV_V_RPS = [];
    SSEw_RPS = 0;
    SSEw_RP = 0;

    indexRP = find(RP_new(:,1) == i);
    RP_Throughput = -RP_new(indexRP,2);
    RP_V     = RP_new(indexRP,3);

    for j = 1: length(RP_Throughput)
    [~,indexo1] = min(abs(OCV_rebuild(:,1)-RP_Throughput(j)));
    OCV_V_RP = [OCV_V_RP; OCV_rebuild(indexo1,2)];
    end

    indexRPS = find(RPS_new(:,1) == i);
    if ~isempty(indexRPS)
        RPS_Throughput = -RPS_new(indexRPS,2);
        RPS_V     = RPS_new(indexRPS,3);
        for k = 1: length(RPS_Throughput)
         [~,indexo2] = min(abs(OCV_rebuild(:,1)-RPS_Throughput(k)));
         OCV_V_RPS = [OCV_V_RPS; OCV_rebuild(indexo2,2)];
        end
        SSEw_RPS = sum(((RPS_V - OCV_V_RPS).^2));
    end

 %SSEw_RP = sum(faktor* ((RP_V - OCV_V_RP).^2));
SSEw_RP = sum((0.5*(RP_V - OCV_V_RP).^2));

    %fnew = ((SSEw_RP +SSEw_RPS)/ faktor*(length(OCV_V_RP)+length(OCV_V_RPS)))^0.5;
    Error = ((SSEw_RP +SSEw_RPS)/ (0.5*length(OCV_V_RP)+length(OCV_V_RPS)));
    fnew = [fnew, Error];
end
fnew =(sum(fnew)/length(fnew))^0.5;


% Compare RMSE bewteen old and new, choose the best(smallest) RMSE value
% and keep the corresponding nest value.
 if fnew<=Value1
    
     Value1   = fnew ;
   
     nestmat = new_nestmat;
    
end

end
