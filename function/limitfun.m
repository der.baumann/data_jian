function x=limitfun(x,lb,ub)
%% Simple constraints
% Apply the lower and upper bound
h1= x-lb<0;
h2= x-ub>0;
x(h1)=lb(h1);
x(h2)=ub(h2);
end
