% This function is for find the informations about relaxation points and
% relaxation point standing.
% sampling rate is 0.1s.
% Jian Hu
% 31/01/2022

function [rp,rps,data] = relax(data,deltaI)

%bascia data and request setting
C = data.current;
V = data.voltage;
T = data.temp;
duration = [];   %initial time duration for relaxation point.

%%%%  relaxation point parameters
rp.voltage =        [];
rp.current =        [];
rp.throughput =     [];
rp.time =           [];
rp.temperature =    [];
rp.duration =       [];
rp.dVdT =           [];
rp.dvdt_end =       [];
rp.dVdT_10 =        [];
rp.DCV_star =       data.DCV;           % Driving Cycle start Voltage
%rp.DCV_start = data.voltage(1);
rp.DCC_star =       data.DCC;           % Driving cycle start Current
%rp.DCC_star = data.current(1);
if length(C) > 200
    data.min_20s = min(C((end-200):end));   % Current in last 20s.
    data.max_20s = max(C((end-200):end));
    data.avg_20s = mean(C((end-200):end));
    data.std_20s = (sum((C((end-200):end)-data.avg_20s).^2)/200)^0.5 ;
else
    data.min_20s = [];
    data.max_20s = [];
    data.avg_20s = [];
    data.std_20s = [];
end
if length(C) > 1800                     % current in last 3min if exist.
data.min_3min = min(C((end-1800):end));
data.max_3min = max(C((end-1800):end));
data.avg_3min = mean(C((end-1800):end));
data.std_3mim = (sum((C((end-1800):end)-data.avg_3min).^2)/200)^0.5 ;
else
data.min_3min = [];
data.max_3min = [];
data.avg_3min = [];
data.std_3mim = [] ;
end
data.min_last =       [];               % initial current after last point.
data.max_last =       [];             
data.avg_last =       [];
data.std_last =       [];

%%%   relaxation point standing parameters
rps.voltage =        [];
rps.current =        [];
rps.time =           [];
rps.temperature =    [];

rps.duration = [];    %% add the RPS duration for quality filtering



% find relaxation point 
point = [];                         % for recording the relaxation point
cut = 1;                            % initail start punkt. 
while 1

    interval = -1;                  % initial interval value.
    flage = 0;
    for i=cut:size(C,2)             % find relaxation point from 'cut'.
        if C(i) >= (-deltaI) && C(i) <= deltaI 
            interval = interval + 1;
            if i == size(C,2) && interval >= 200      %Jump out of the while loop after the entire piece of data ends, if the requirements are met
                duration = [duration,interval/10];    % record the duration for this relaxation point. [second]
                point = [point,i];                    % record the end point for this relaxation point.
                flage = 1;
                break
            elseif i == size(C,2) && interval <= 200   %Jump out of the while loop after the entire piece of data ends, if the requirements are not met
                flage = 1;
                break
            else
                continue;
            end
        end

        if i == size(C,2)                              %Jump out of the while loop after the entire piece of data ends, if the requirements are not met
            flage = 1;
            break
        end
      
        if  interval >= 200                            %Jump out of the for loop , if the requirements are not met and
            duration = [duration,interval/10];         % for the next for loop starts with the next punkt('cut +1').
           point = [point,i-1];
           cut = i +1;
           break
        elseif interval < 200
            cut = i+1;
            break
        end
    end
    if flage == 1
        break;
    end
end


% relaxation point standing
if C(1)< deltaI && C(1) > (-deltaI)
    relaxation = 1;

else
    relaxation = 0;
end

%throughput
Ah = [];
deltaAh = 0;
for i = 1:size(C,2)
    deltaAh = deltaAh + 0.1*C(i)/3600;
    Ah = [Ah,deltaAh];
end

% determination dV/dT and ....
% if in this trip not have any point, thhe value is [].

dVdT =[];
dvdt_end =[];
dVdT_10 =[];
if length(point) ~= 0
dVdT = zeros(1,length(point));
dvdt_end = zeros(1,length(point));
dVdT_10 = zeros(1,length(point));

for i = 1:size(point,2)
   dVdT(1,i) =           (V(point(i))-V(point(i)-duration(i)*10))/(duration(i)/10);
   dvdt_end(1,i) =       abs((V(point(i))-V(point(i)-1))/0.1);
   dVdT_10 (1,i)=        (V(point(i))-V(point(i)-100))/10;
end
end

%parameters determination
% relaxation point
data.Ah = deltaAh;
data.duration = size(C,2)/10;
rp.voltage =        V(point);
rp.current =        C(point);
rp.throughput =     Ah(point);
rp.time =           data.start_time +seconds(0.1.*(point-1));
rp.temperature =    T(point);
rp.duration =       duration;
rp.dVdT =           dVdT;
rp.dvdt_end =       dvdt_end;
rp.dVdT_10 =        dVdT_10;
rp.DCV_star =       data.DCV;
rp.DCC_star =       data.DCC;
if length(point) ~= 0
data.min_last =       min(C(point(end):end));
data.max_last =       max(C(point(end):end));
data.avg_last =       mean(C(point(end):end));
data.std_last =       (sum((C(point(end):end)-data.avg_last).^2)/200)^0.5;
end

%relaxation point standing
if relaxation == 1
    rps.voltage =        V(1);
    rps.current =        C(1);
    rps.time =           data.start_time;
    rps.temperature =    T(1);
    rps.duration = (min(find(C > deltaI))-1)*0.1; %% for quality filtering
    data.point = [1 point]; % record the point data
else
    data.point = point;
end


end





%%%%%%%plot
%x = point;
%y = C(point);
%yyaxis left
%plot(data.current)
%text(x,y,'o','Color','k')
%hold on
%yyaxis right
%plot(V)
