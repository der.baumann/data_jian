
clear

%% delet the trip with no data by your hands
subdir = dir('C:\Users\wli-jhu\Desktop\moritz\Trips2019');
b = [];
a = [];
for i = 3:length(subdir)
    load (['C:\Users\wli-jhu\Desktop\moritz\Trips2019\' subdir(i).name]);
    if isempty(data.voltage) || isempty(data.current)
        b = [b; subdir(i).name];
    end
    if isempty(find(data.voltage >= 330))
        a = [a; subdir(i).name];
    end
end

%% delete the data of volatge wich is smaller than EOD.

subdir = dir('C:\Users\wli-jhu\Desktop\moritz\Trips2019');
for i = 3:length(subdir)
     load (['C:\Users\wli-jhu\Desktop\moritz\Trips2019\' subdir(i).name]);
     if data.voltage(end) <=330  % hier should change to the real EOD.
       index = max(find(data.voltage >= 330))
       data.voltage = data.voltage(1:index);
       data.current = data.current(1:index);
       data.endV  = data.voltage(end);
       data.endC = data.current(end);
        file = ['moritz\Trips2019/' subdir(i).name];
    save (file,"data")
     end
end


%% Hier can u find the trips with lower SOC and to check the Trip with the mark.
subdir = dir('C:\Users\wli-jhu\Desktop\moritz\Trips2021');
maxSoc = [];
SOCvalue = [];
for i = 3:length(subdir)
     load (['C:\Users\wli-jhu\Desktop\moritz\Trips2021\' subdir(i).name]);
     if data.soc(end) <= 60
         maxSoc = [maxSoc; subdir(i).name];
         SOCvalue = [ SOCvalue, data.soc(end)];
     end
end
