% This Skrip is for calculating R0;
% The required data is the complete current and voltage data of each trip;
% R0_trace records the R0 value of all trips;
% Jian Hu
% 29/1/2022

clear;
clc;

R0_trace = []; % Track R0 changes across all trips
subdir = dir('C:\Users\mikeh\OneDrive\桌面\SOH\SOHC\data1')
t = [];  % timedate
T = [];  % Temperature（mean)

    for i = 3:length(subdir)
        load (['C:\Users\mikeh\OneDrive\桌面\SOH\SOHC\data1/' ...
                subdir(i).name]);  %load the trip data
        C01 = data.current;                %Current data with sampling-rate 0.1s.
        C05 = data.current(1:5:end);       %Current data with sampling-rate 0.5s.
        C1 = data.current(1:10:end);       %Current data with sampling-rate 1s.
        C10 = data.current(1:100:end);     %Current data with sampling-rate 10s.
        V01 = data.voltage;                %Voltage data with sampling-rate 0.1s.
        V05 = data.voltage(1:5:end);
        V1 = data.voltage(1:10:end);
        V10 = data.voltage(1:100:end);
        C = C1;                            % hier choose the sampling-rate to the data,which will  be analysed.
        V = V1;
        tpunkt = [];                       % Mark the points that meet the requirements
        for k = 2:length(C)
            if abs(C(k)-C(k-1)) >= 30      % when the delta Current bigger than 30A, mark the point as tpunkt.
                tpunkt = [tpunkt,k];
            end
        end
        if size(tpunkt,1) == 0             % when this trip has no point, than ignore this trip.
            continue;
        end
        deltaC = C(tpunkt)' - C((tpunkt-1))';
        deltaV = V(tpunkt)' - V((tpunkt-1))';
        R_vor = deltaV./deltaC;             % calculate the row Resistance.
        R0 = movmean(R_vor,length(R_vor));  % use the move mean to reduce the influence of extreme values
        R0_mean = mean(R0);                 % Take the mean as R0
        save([ 'C:\Users\mikeh\OneDrive\桌面\SOH\SOHR/results_data1/' subdir(i).name],...
            "R0_mean","R_vor","deltaC",...
            "deltaV","tpunkt","R0","V","C")
        R0_trace = [R0_trace, R0_mean];   % show the change of Resistance of all trips
        t = [t,data.start_time];
        T = [T,mean(data.temp(10:end))];          % show the change of Temperature of all trips
    end

    R(:,1) = T;
    R(:,2) = R0_trace

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure (1)
plot(t,R0_trace)
title 'R0'

figure (2)
plot(R(:,1),R(:,2))
xlabel 'Temperature';
ylabel 'R0'
title '2020'


%hu = movmean(R0_trace,5);
%yyaxis left
%plot(C)
%yyaxis right
%plot(V)
%grid on

