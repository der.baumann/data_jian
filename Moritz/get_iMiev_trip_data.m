%Function to get iMIEV data of trips from Matlab File directory
%and save to export

function [] = get_iMiev_trip_data(source, export)

    subdir = dir(source);
    for a=3:length(subdir)-1
        load([source '\' subdir(a).name], 'Current_Batt','Voltage_Batt','Start_time', 'Temp_Battery_1');
        data.voltage = Voltage_Batt(~isnan(Voltage_Batt));
        data.current = Current_Batt(~isnan(Voltage_Batt));
        data.temp = Temp_Battery_1(~isnan(Voltage_Batt));
        data.start_time = datetime(Start_time);
        if ~isempty(data.voltage)
            data.endV=data.voltage(end);
            data.endC=data.current(end);
            data.stC = data.current(1);
            data.stV = data.voltage(1);
        end
        
        save([export '\Trip_File_' num2str(a-2) '.mat'],'data');
    end
    
    
end